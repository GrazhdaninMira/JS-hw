
// функція-конструктор
function Human(name, nationality, age, gender) {
    
    ++Human.maxCount;
    
    this.name = name;
    this.nationality = nationality;
    this.age = age;
    this.gender = gender;  

    if (Human.maxCount == 1) {
        
        return this.winner = "you are the winner"; 
    }

}

// статична властивість
Human.maxCount = 0;


// статичний метод
Human.getDiscount = function () {
        return new Human("Hanna", "bolgarian", "28", "female");
    }
     
// метод на прототипі
Human.prototype.adult = function() {
    const answ = "ukrainian"; 
        if (this.nationality !== answ) {
            alert("Этот сервис только для граждан Украины");
        }
        else {
            alert(`На вашем счету ${this.score.uah} грн, ${this.score.usd} доларів, ${this.score.eur} євро`)
        }
}

// властивість на прототипі
Human.prototype.score = {
        uah: "10000",
        usd: "2500",
        eur: "1700"
    }

// створення екземпляру (об'єкт human)
const human = new Human(prompt("Введите ваше имя"), prompt("Введите вашу национальность"), prompt("Введите ваш возраст"), prompt("Введите ваш пол"));

// виклик методу екземпляра
human.adult();

// виклик ф-цій по ланцюжку
Human.getDiscount().adult();

