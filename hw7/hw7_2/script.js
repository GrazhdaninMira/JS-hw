
function createNewUser (firstName, lastName, birthday) {
    return {
        firstName,
        lastName,
        birthday,

        getLogin() {
            const name = this.firstName[0] + this.lastName;
            return name.toLowerCase();
        },

        getAge() {
            const year = this.birthday.slice(6, 10);
            const age = 2022 - Number(year);
            return age;
        }
    }
}

const userFirstName = prompt("Введите ваше имя");
const userLastName = prompt("введите вашу фамилию");
const userBirthday = prompt("введите дату вашего рождения в формате dd.mm.yyyy", "26.06.1989")


let newUser = createNewUser(userFirstName, userLastName, userBirthday);
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
