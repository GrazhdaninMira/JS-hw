var x = 6, y = 14, z = 4;

x += y - x++ * z;
/* 
x++ = 6;
x++ * z = 24;
y - 24 = -10;
x - 10 = -4;
*/
document.write(x + '</br>');

var x = 6, y = 14, z = 4;

z = --x - y * 5;
/* 
--x = 5;
y * 5 = 70;
--x - 70 = -65;
*/
document.write(z + '</br>');

var x = 6, y = 14, z = 4;

y /= x + 5 % z;
/* 
5 % z = 1;
x + 1 = 7;
y / 7 = 2;
*/

document.write(y + '</br>');

var x = 6, y = 14, z = 4;

var res = z - x++ + y * 5;
/* 
x++ = 6;
y * 5 = 70;
z - x++ + 70 = 68;
*/
document.write(res + '</br>');

var x = 6, y = 14, z = 4;

x = y - x++ * z;
/* 
x++ = 6;
x++ * z = 24;
y - 24 = -10;
*/
document.write(x);