

window.addEventListener("DOMContentLoaded", () => {

    const keyBoard = document.querySelector(".keys");
    const input = document.querySelector(".display > input");
    const equalKey = document.querySelector("input.orange");
    const reset = document.querySelector("p:last-child > input:nth-child(3)");
    const point = document.querySelector("p:last-child > input:nth-child(2)");
    const mMinus = document.querySelector("p:first-child > input:nth-child(2)");
    const mPlus = document.querySelector("p:first-child > input:nth-child(3)");
    const mrc = document.querySelector("p:first-child > input:first-child");

    const mIcon = document.createElement("span");
    mIcon.classList.add("m-icon");
    document.querySelector(".display").append(mIcon);


    let operand;
    let previousValue;
    let action;
    let result;
    let memory = 0;
    let checkDoubleMrc;


    outputSelectKey = (e) => {

        //если нажатие происходит по одной из кнопок. Отсеиваем случайные нажатия вне кнопок.
        if (e.target.value) {
            const currentKeyValue = e.target.value;
            const patternOperand = /^[*/+-]/;
            const patternPoint = /[.]/
            checkDoubleMrc++;

            // функция, выполняющая расчет математических операций.
            function getResult() {

                //если previousValue - не пустая строка.
                if (previousValue) {
                    switch (action) {

                        case "+":
                            result = Number(previousValue) + Number(input.value);
                            break;

                        case "-":
                            result = Number(previousValue) - Number(input.value);
                            break;

                        case "*":
                            result = Number(previousValue) * Number(input.value);
                            break;

                        case "/":
                            result = Number(previousValue) / Number(input.value);
                            break;
                    }
                    previousValue = "";
                    input.value = result;
                }

            }

            //функция проверки на наличие точки в выражении в input. Если она уже есть - ее невозможно добавить вторую. 
            function checkDoublePoint() {
                //условие, которое не позволяет добавлять в input более одной точки.
                patternPoint.test(input.value) ? point.disabled = true : point.disabled = false;
            }
            
            //функция сброса инпута и переменных, хранящих операнд и предыдущее число. 
            function resetInput() {
                input.value = "0";
                previousValue = "";
                operand = undefined;
            }


            //выполнение арифметической операции после нажатия на один из арифметических операндов +, -, *, /.
            if (previousValue && operand == undefined && patternOperand.test(currentKeyValue)) {

                getResult();

            }



            //если пользователь нажал на одну из цифр
            if (Number.isInteger(Number(currentKeyValue)) || currentKeyValue == ".") {

                //условие, которое не позволяет выводить в input.value дополнительные нули.
                if (input.value == "0" && currentKeyValue != ".") {
                    input.value = currentKeyValue;
                }


                //если перед введенной цифрой не были введены математические операнды +, -, *, /, в input.value добавляется эта введенная цифра.
                else if (operand == undefined) {
                    input.value += currentKeyValue;

                }

                //если перед введенной цифрой был введен один из математических операндов +, -, *, /, значение input.value сохраняется в переменную previousValue, значение operand сохраняется в переменную action и обнуляется. input.value также обнуляется.
                else {
                    previousValue = input.value;
                    action = operand;
                    operand = undefined;
                    input.value = "";
                    input.value += currentKeyValue;
                    equalKey.disabled = false;
                }
            }

            checkDoublePoint();

            //выполнение арифметической операции при нажатии на кнопку "=".
            if (currentKeyValue == equalKey.value && operand == undefined) {

                getResult();

            }

            //при нажатии на кнопку одного из математических операндов +, -, *, /, присвоение перменной operand его значения.
            if (patternOperand.test(currentKeyValue)) {
                operand = currentKeyValue;
            }

            //если пользователь нажал на кнопку "cброс"
            if (currentKeyValue == reset.value) {
                resetInput();
            }
            
            //условие, описывающее логику работы m+, m-
            if (currentKeyValue == mMinus.value || currentKeyValue == mPlus.value) {
                mIcon.textContent = "m";

                if (currentKeyValue == mPlus.value) {
                    getResult();
                    memory += Number(input.value);
                    resetInput();
                }

                else if (currentKeyValue == mMinus.value) {
                    getResult();
                    memory -= Number(input.value);
                    resetInput();
                }

            }
            
            //условие, описывающее логику работы mrc
            else if (currentKeyValue == mrc.value) {
                

                if (checkDoubleMrc == 2) {
                    memory = 0;
                    debugger;
                    mIcon.textContent = "";
                    checkDoubleMrc = false;
                }

                //на каждом очередном событии checkDoubleMrc увеличивается на 1, поэтому сброс памяти произойдет только при последовательном повторном нажатии кнопки mrc.
                else {
                    input.value = memory;
                    checkDoubleMrc = 1;
                }        
            }
        }

    }

    //вызов события
    keyBoard.addEventListener("click", outputSelectKey, false);

});



