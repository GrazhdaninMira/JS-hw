
window.onload = () => {
    // кнопка, инициирующая начало взаимодействия.
    const button = document.getElementById("button");

    button.onclick = () => {

        //создание и вывод инпута и кнопки.
        const input = document.createElement("input");
        const drawButton = document.createElement("input");
        input.setAttribute = ("placeholder", "введите диаметр круга");
        drawButton.type = "button";
        drawButton.value = "Нарисовать";

        button.replaceWith(input);
        input.after(drawButton);

        //создание кружков (100шт) по клику на кнопку
        drawButton.onclick = () => {

            const ringContainer = document.createElement("div");
            ringContainer.classList.add("container");
            ringContainer.style.width = `${10 * input.value}px`;
            drawButton.after(ringContainer);

            for (let i = 0; i < 100; i++) {
                const oneRing = document.createElement("div");
                oneRing.style.width = input.value + "px";
                oneRing.style.height = input.value + "px";
                oneRing.style.backgroundColor = randomColor();
                oneRing.classList.add("one-ring");
                ringContainer.prepend(oneRing);

                //ф-ция, удаляющая єлемент, по которому было нажатие
                oneRing.onclick = (e) => {
                    e.target.remove();
                }
            }

            //ф-ция выбора случайного цвета
            function randomColor() {
                const randomColor = Math.floor(Math.random() * 360);
                return `hsl(${randomColor}, 50%, 50%)`
            }
        }
    }
}
