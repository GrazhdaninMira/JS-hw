//прямоугольник

for (i = 0; i < 20; i++) {
    for (j = 0; j < 100; j++) {
        document.write ("*");
    }
    document.write ("</br>");
}

document.write ("</br>");



//прямоугольный треугольник

for (i = 0; i < 20; i++) {
    
    for (j = 0; j <= 2 * i; j++) {
        
        document.write ("*");
    }
    document.write ("</br>");
}

document.write ("</br>");



//равносторонний треугольник (в лучшем случае, равнобедренный. И то, если не пробелы, а решетки использовать)

let k = 15;
for (i = 0; i < k; i++) {
    
    for (j = 0; j <= (2 * k - 1); j++) {
        
        if (j >= k - i && j <= k + i) {
            document.write ('*')
        }
        else {
        document.write ('#');
        }
    };
    document.write ("</br>");

};

document.write ("</br>");



//ромб

k = 15;

for (i = 0; i < 2 * k + 1; i++) {
    
    for (j = 0; j <= (2 * k); j++) {
        
        if (i <= k) {
            if (j >= k - i && j <= k + i) {
                document.write ('*')
            }
            else {
            document.write ('#');
            }
        };

        if (i > k) {
            if (j >= i - k && j <= 3 * k - i) {
                document.write ('*')
            }
            else {
            document.write ('#');
            }
        };
    };
    document.write ("</br>");

}

document.write ("</br>");
