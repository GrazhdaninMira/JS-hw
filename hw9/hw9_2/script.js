window.onload = () => {

    const input = document.createElement("input");
    input.type = "text";
    input.placeholder = "Введите номер телефона";
    document.body.prepend(input);

    const button = document.createElement("input");
    button.type = "submit";
    button.value = "Сохранить";
    input.after(button);

    const error = document.createElement("div");
    error.textContent = "Введите номер корректно, пожалуйста";

    const pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;

    button.onclick = () => {

        if (pattern.test(input.value)) {

            document.body.classList.add("green");

            document.location = "https://google.com";

        }

        else {

            input.before(error);

            hideError = () => {
                document.body.removeChild(error);
            }

            setTimeout(hideError, 2000);

        }

    }

}