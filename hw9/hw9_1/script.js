
window.onload = () => {

    let start;

    let secondsCounter = document.querySelector(".stopwatch-display > span:last-child").textContent;
    let minuteCounter = document.querySelector(".stopwatch-display > span:nth-child(2)").textContent;
    let hoursCounter = document.querySelector(".stopwatch-display > span:first-child").textContent;
    const background = document.querySelector(".container-stopwatch");
    
    
    document.querySelector(".stopwatch-control > button:first-child").onclick = () => {
        //debugger;
        
        background.classList.add("green");
        background.classList.remove("black");

        
        const pattern1 = /[1-9]{1}/;
        const pattern2 = /\d{2}/;

        const secondsCount = () => {

            ++secondsCounter;

            if (secondsCounter > 59) {
                document.querySelector(".stopwatch-display > span:last-child").textContent = "00"
                secondsCounter = 0;

                ++minuteCounter;

                if (minuteCounter > 59) {
                    document.querySelector(".stopwatch-display > span:nth-child(2)").textContent = "00"
                    minuteCounter = 0;

                    ++hoursCounter;

                    if (hoursCounter > 23) {
                        document.querySelector(".stopwatch-display > span:first-child").textContent = "00";
                        hoursCounter = 0;

                    }

                    else if (hoursCounter === "00") {
                        document.querySelector(".stopwatch-display > span:first-child").textContent = "0" + hoursCounter;
                    }

                    else if (pattern2.test(hoursCounter)) {
                        document.querySelector(".stopwatch-display > span:first-child").textContent = hoursCounter;
                    }

                    else if (pattern1.test(hoursCounter)) {
                        document.querySelector(".stopwatch-display > span:first-child").textContent = "0" + hoursCounter;
                    }

                }

                else if (minuteCounter === "00") {
                    document.querySelector(".stopwatch-display > span:nth-child(2)").textContent = "0" + minuteCounter;
                }

                else if (pattern2.test(minuteCounter)) {
                    document.querySelector(".stopwatch-display > span:nth-child(2)").textContent = minuteCounter;
                }

                else if (pattern1.test(minuteCounter)) {
                    document.querySelector(".stopwatch-display > span:nth-child(2)").textContent = "0" + minuteCounter;
                }

            }

            else if (secondsCounter === "00") {
                document.querySelector(".stopwatch-display > span:last-child").textContent = "0" + secondsCounter;
            }

            else if (pattern2.test(secondsCounter)) {
                document.querySelector(".stopwatch-display > span:last-child").textContent = secondsCounter;
            }

            else if (pattern1.test(secondsCounter)) {
                document.querySelector(".stopwatch-display > span:last-child").textContent = "0" + secondsCounter;
            }


        }

        start = setInterval(secondsCount, 1000);

    }

    document.querySelector(".stopwatch-control > button:nth-child(2)").onclick = () => {

        background.classList.add("red");
        background.classList.remove("green");

        clearInterval(start);

    }

    document.querySelector(".stopwatch-control > button:last-child").onclick = () => {

        background.classList.add("silver");
        background.classList.remove("green");
        background.classList.remove("red");

        clearInterval(start);

        document.querySelector(".stopwatch-display > span:last-child").textContent = "00"
        secondsCounter = 0;

        document.querySelector(".stopwatch-display > span:nth-child(2)").textContent = "00"
        minuteCounter = 0;

        document.querySelector(".stopwatch-display > span:first-child").textContent = "00";
        hoursCounter = 0;

    }

}