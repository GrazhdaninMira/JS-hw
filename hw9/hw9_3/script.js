window.onload = () => {

    const container = document.createElement("div");
    container.classList.add("container");
    document.body.prepend(container);
    
    const urlArray = [
        "https://oir.mobi/uploads/posts/2021-06/1623108205_47-oir_mobi-p-samie-krasivie-peizazhi-priroda-krasivo-fo-48.jpg",
        "https://oir.mobi/uploads/posts/2021-06/1623108202_25-oir_mobi-p-samie-krasivie-peizazhi-priroda-krasivo-fo-26.jpg",
        "https://oir.mobi/uploads/posts/2021-06/1623108236_18-oir_mobi-p-samie-krasivie-peizazhi-priroda-krasivo-fo-19.jpg",
        "https://oir.mobi/uploads/posts/2021-06/1623108234_12-oir_mobi-p-samie-krasivie-peizazhi-priroda-krasivo-fo-13.jpg",
        "https://oir.mobi/uploads/posts/2021-06/1623108225_34-oir_mobi-p-samie-krasivie-peizazhi-priroda-krasivo-fo-35.jpg"
    ];
    
    const imgArray = [];
    
        for (i = 0; i < urlArray.length; i++) {
            const img = document.createElement("img");
            img.classList.add("picture");
            img.setAttribute ("src", urlArray[i]);
            imgArray.push(img);
        }

    let count = 0;
    
    function changeImage() {
        
        if (count >= urlArray.length) {
            count = 0;
        }
        container.prepend(imgArray[count]);

        if (count < urlArray.length) {
            container.replaceChildren(imgArray[count]);
        }

        count++;
    }

    setInterval(changeImage, 3000);

}