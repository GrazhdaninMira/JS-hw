//Вариант решения задачи без помощи callback функции
/*
const add = (a = 1, b = 1) => {
    return a + b;
}

const multiple = (a = 0, b = 0) => {
    return a * b;
}

const subtraction = (a = 0, b = 0) => {
    return a - b;
}

const division = (a = 0, b = 0) => {
    return a / b;
}

let num1 = prompt("введите первое число");
let num2 = prompt("введите второе число");
let oper = prompt("введите, какую математическую операцию хотите произвести");

if (num1 === "0" || num2 === "0") {
    console.error("ошибка");
}

else if (num1 === "" || num2 === "") {
    alert("вы не ввели одно из чисел");
}

else if (oper === "+") {
    alert(`результат вычисления: ${add(parseFloat(num1), parseFloat(num2))}`);
}

else if (oper === "*") {
    alert(`результат вычисления: ${multiple(parseFloat(num1), parseFloat(num2))}`);
}

else if (oper === "-") {
    alert(`результат вычисления: ${subtraction(parseFloat(num1), parseFloat(num2))}`);
}

else if (oper === "/") {
    alert(`результат вычисления: ${division(parseFloat(num1), parseFloat(num2))}`);
}

*/


// Вариант решения с callback функцией

const add = (a = 1, b = 1) => {
    return a + b;
}

const multiple = (a = 0, b = 0) => {
    return a * b;
}

const subtraction = (a = 0, b = 0) => {
    return a - b;
}

const division = (a = 0, b = 0) => {
    return a / b;
}


let calculate = function (a, b, callback) {

    if (arguments.length === 3) {
    
        if (callback === add) {
            return add(a, b);
        }

        else if (callback === multiple) {
            return a * b;
        }

        else if (callback === subtraction) {
            return a - b;
        }

        else if (callback === division) {
            if (b === 0) {
                console.error("ошибка");
            }
            else {
            return a / b;
            }
        }
}
    else {
        console.error("ошибка: задайте 3 аргумента для callback функции")
    }
}

let result = calculate (5, 6, division, 4);

